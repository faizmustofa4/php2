<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh Conditional</h2>
    <?php
            /*
            Soal No 1
            Greetings
            Buatlah sebuah function greetings() yang menerima satu parameter berupa string.

            contoh: greetings("abduh");
            Output: "Halo Abduh, Selamat Datang di PKS Digital School!"
            */

            // Code function di sini
            function greetings($nama){
                echo "Halo " . $nama . ", Selamat Datang di PKS Digitak Schol!<br>";
            }

            // Hapus komentar untuk menjalankan code!
            greetings("Bagas");
            greetings("Wahyu");
            greetings("Abdul");

            echo "<br>";

                        /*
            Soal No 2
            Reverse String
            Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping
            (for/while/do while).
            Function reverseString menerima satu parameter berupa string.
            NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

            reverseString("abdul");
            Output: ludba

            */

            // Code function di sini

            function reverse($kata1){
                $panjangkata = strlen($kata1);
                $tampung = "";
                for($i=($panjangkata - 1); $i>=0; $i--){
                    $tampung .= $tampung[$i];
                }
                return $tampung;
            }
            
            function reverseString($kata2){
                $string=reverse($kata2);
                echo $string ."<br>";
            }

            // Hapus komentar di bawah ini untuk jalankan Code
            reverseString("abduh");
            reverseString("Digital School");
            reverseString("We Are PKS Digital School Developers");
            echo "<br>";



    ?>
</body>
</html>
