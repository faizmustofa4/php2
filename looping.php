<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php
    echo "<h3>Soal Pertama</h3>";
    
    for($i=2; $i<=20; $i+=2){
        echo $i . " - I Love PHP<br>";
    }

    echo "<h3>Soal Kedua</h3>";
    for($a=20; $a>=2; $a-=2){
        echo $a . " - I Love PHP<br>";
    }

    echo "<h3>Soal Nomor 2  </h3>";
    $number = [18, 45, 29, 61, 47, 34];
    echo "Array Angka : ";
    print_r($number);
    echo "<br>";
    echo " Hasil sisa dari Angka : ";
    foreach ($number as $value) {
        $rest[] = $value %=5;
    }
    print_r($rest);

    echo "<h3>Soal Ketiga</h3>";

    $items = [
        ["001", "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"], 
        ["002", "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
        ["003", "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
        ["004", "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"],
    ];

    foreach($items as $key => $value){
        $biodata = array(
            'kode' => $value[0],
            'Nama Barang' => $value[1],
            'Harga' => $value[2],
            'Komentar' => $value[3],
            'merek' => $value[4]
        );
        print_r($biodata);
        echo "<br>";
    }
    echo "<h3>Soal keempat</h3>";
    for($e=1; $e<=5; $e++){
        for($b=1; $b<=$e; $b++)
        {
            echo "*";
        }
        echo "<br>";
    }



?>
</body>
</html>
